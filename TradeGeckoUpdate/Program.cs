﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using TradeGeckoSharp.Services;
using TradeGeckoSharp.Entities;
using TradeGeckoSharp.Filters;

namespace TradeGeckoUpdate
{
    class Program
    {
        private static TradeGeckoVariantService TGService;
        private static TradeGeckoVariant u_variant = null;

        static async void UpdateName()

        {
            System.Threading.Thread.Sleep(1000);
            var n = await TGService.UpdateAsync(u_variant);
            if (!n)
            {
                Console.WriteLine("Update not successful");
                Console.ReadLine();
            }
        }

        static void Main(string[] args)
        {

            TGService = new TradeGeckoVariantService("https://api.tradegecko.com/", "b08a013c3297151fa6eac2890d008cdff4b93121ad91a032f372c8cd4b63b232");
            var currentPage = 1;
            bool complete = false;

            while (!complete)
            {

                Console.WriteLine("************** Current page " + currentPage.ToString());

                System.Threading.Thread.Sleep(1000);            
                var variants = TGService.ListAsync(new TradeGeckoVariantFilter() { Page = currentPage }).Result.ToList();

                foreach (TradeGeckoVariant variant in variants)
                {
                    Console.WriteLine("PName: " + variant.ProductName + "Name: " + variant.Name + " Options: " + variant.Opt1 + " " + variant.Opt2 + " " + variant.Opt3);
                    var nname = variant.Opt1;
                    if (!String.IsNullOrEmpty(variant.Opt2)) nname = nname + " / " + variant.Opt2;
                    if (!String.IsNullOrEmpty(variant.Opt3)) nname = nname + " / " + variant.Opt3;
                    if (variant.Name != nname)
                    {
                        Console.WriteLine("         Changing name to " + nname);
                        u_variant = variant;
                        u_variant.Name = nname;
                        UpdateName();
                    }
                }
                complete = !variants.Any();
                currentPage++;

            }
            Console.ReadLine();
        }
    }
}
